console.log

const details = {
    MFName: "Victor",
    MLName: "Garlitos",
    Age: 18,
    Hobbies : [
        "playing", "eating", "spleeping"
    ] ,
    workAddress: {
        housenumber: "Block 18, Lot 32",
        street: "Tarago Street",
        city: "Caloocan City",
        state: "State of the nation",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.MFName)
console.log("My Last Name is " + details.MLName)
console.log(`Yes, I am ${details.MFName} ${details.MLName}.`)
console.log("I am " + details.Age + " years old.")
console.log(`My hobbies are ${details.Hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");